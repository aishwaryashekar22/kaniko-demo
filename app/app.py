from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello():
    return '<h1>Hi there! This is a very simple test for Flask in a container</h2>'


if __name__ == "__main__":
    app.run(debug=True)